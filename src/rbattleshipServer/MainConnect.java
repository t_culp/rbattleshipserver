package rbattleshipServer;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Date;

public class MainConnect {
	
	private static ArrayList<ConnectedClient> connectedClients = new ArrayList<ConnectedClient>(); // all clients connected to the server, in or out of a game
	private static ArrayList<ConnectedClient[]> inGameClients = new ArrayList<ConnectedClient[]>(); //pairs of clients in a game
	private static ArrayList<BannedClient> bannedClients = new ArrayList<BannedClient>(); // contains a list of banned usernames and IPs
	private static int takeANumber = 0; // number assigned to a client waiting to be paired
	private static int nowServing = 1; // pairing the client with the current number (see takeANumber)
	private static Object assignNumberLock = new Object(); // used to ensure that only one person is incrementing and getting assigned a number at a time
	private static ConnectedClient[] openPair = null; // temporary pair for matching two clients for a game
	private static boolean secondClientConnected = false; // used for telling the first client connected to wait
	private long startTime; // used for finding good times to do a server reset/keep track of various things (developing feature)
	private static MainConnect server; // instance of this class
	private static boolean waitingOnFirstClientToEndHeartbeat; // second client connected will wait until the heartbeat is finished
	private static ServerFrontEnd gui;
	private static ServerSocket serverSocket;
	
	private enum serverCommand {
		BAN       ("Revokes access to server and kicks a client off the server based on IP number.", "/ban <username>"), 
		UNBAN     ("Allows a client at the banned IP address to rejoin the server.", "/unban <username>"), 
		HELP      ("Displays information about each command.", "/help"), 
		TIME      ("Displays the amount of time the server has been online.", "/time"), 
		CHECKBANS ("Displays the banned usernames (for purposes of unbanning).", "/checkbans"),
		MSG       ("Sends a message to a single user.", "/msg <username> <message>");
		
		private final String description;
		private final String usage;
		
		private serverCommand (String description, String usage) {
			this.description = description;
			this.usage = usage;
		} // close constructor
		
		private String description() {
			return description;
		} // close description
		
		private String usage() {
			return usage;
		} // close usage
		
	} // close serverCommand
	
	private enum clientCommand { 
		HELP        ("Displays information about each command.", "/help"),
		ONLINEUSERS ("Displays a list of current online users.", "/onlineusers"),
		MSG         ("Sends a message to a single user.", "/msg <username> <message>");
		
		private final String description;
		private final String usage;
		
		private clientCommand (String description, String usage) {
			this.description = description;
			this.usage = usage;
		} // close constructor
		
		private String description() {
			return description;
		} // close description
		
		private String usage() {
			return usage;
		} // close usage
		
	} // close clientCommand
	
	public static void main(String[] args) {
		Thread.currentThread().setName("SERVER LOGIN");
		server = new MainConnect();
		gui = new ServerFrontEnd(server);
		server.startServer(); // sets up a server socket
		if (serverSocket != null)
			server.connectClients(); // begins a loop which will continuously connect clients
	} // close main
	
	class Restart implements Runnable {
		
		public void run() {
			server.startServer();
			if (serverSocket != null)
				server.connectClients();
		} // close run
		
	} // close Restart
	
	private void startServer() {
		boolean invalidPort = true;
		int port = 2014;
		// this loop checks to make sure that a port with a reasonable number is chosen
		while (invalidPort) {
			String input = gui.getPort();
			try { // This try makes sure that the user doesn't enter anything weird (letters, decimals, etc)
				if (input == null) {
					return;
				}
				if (input.equals("")) {
					invalidPort = false;
				} else {
					port = Integer.parseInt(input);
					if (port < 1024 || port > 65535) {
						gui.showErrorDialog("Please input a valid port number");
					} else {
						invalidPort = false;
					}
				}
			} catch (NumberFormatException e) {
				gui.showBadPortNumber();
			}
		}
		try {
			serverSocket = new ServerSocket(port);
		} catch (BindException e) { // Caught if anything else is using the port (most often another running server)
			gui.showErrorDialog("Could not acquire socket because an application is using port " + port + ".");
			return;
		} catch (IOException ex) { // The catch-all, probably won't happen and it's unknown what or why would cause this error. See stack trace.
			gui.showErrorDialog("Could not acquire socket for unknown reasons.");
			ex.printStackTrace();
			return;
		}
		startTime = System.currentTimeMillis();
		gui.setRunning(true);
		gui.updateChatBox("[**]: Battleship server has started normally on " + (new Date()).toString() + "\n\tuse \"/help\" for information");
	} // close startServer
	
	private void connectClients() {
		try {
			while (true) { // This loop will run until the server is terminated, its job being to start the login process
				Socket clientSocket = serverSocket.accept();
				ConnectedClient client = new ConnectedClient(clientSocket);
				Thread login = new Thread(new AssignUsername(client));
				login.setName("Login and Connect Thread");
				login.start();
			} // close while(true)
		} catch (SocketException e) {
		} catch (IOException ex) { // something strange happened.
			ex.printStackTrace();
			gui.updateChatBox("[**]: Failed to connect client.");
			connectClients();
		}
	} // close connectClients
	
	class AssignUsername implements Runnable {
		
		ConnectedClient client;
		
		public AssignUsername(ConnectedClient c) {
			client = c;
		} // close constructor
		
		// The entire point of this function is to get a username and throw the client into the pool of those waiting to be connected to other clients
		public void run() {
			// get username
			boolean continueCommunicatingWithClient = true;
			ObjectOutputStream clientOutput = client.getOutput();
			int i = 0;
			while (i < bannedClients.size() && continueCommunicatingWithClient) {
				if (client.getIP().equals(bannedClients.get(i).getIP())) {
					continueCommunicatingWithClient = false;
					try {
						clientOutput.writeBoolean(false);
						clientOutput.flush();
					} catch (IOException e) {}
				}
				++i;
			}
			String username = null;
			if (continueCommunicatingWithClient) {
				boolean validUsername = false;
				ObjectInputStream clientInput = client.getInput();
				try {
					clientOutput.writeBoolean(true);
					clientOutput.flush();
					while (!validUsername) {
						validUsername = true;
						username = (String) clientInput.readObject();
						i = 0;
						while (i < connectedClients.size() && validUsername) {
							if (username.equalsIgnoreCase(connectedClients.get(i).getUsername()) || username.length() < 2 || username.length() > 15 || username.contains(" ") || username.equalsIgnoreCase("server")) {
								// signal to client to attempt username again because of bad criteria
								String usernames = "";
								if (connectedClients.size() > 2) {
									for (int j = 0; j < connectedClients.size() - 1; j++) {
										usernames += connectedClients.get(j).getUsername() + ", ";
									}
									usernames += " and " + connectedClients.get(connectedClients.size() - 1).getUsername() + ".";
								} else if (connectedClients.size() == 2) {
									usernames = connectedClients.get(0).getUsername() + " and " + connectedClients.get(1).getUsername();
								} else {
									usernames = connectedClients.get(0).getUsername();
								}
								clientOutput.writeBoolean(false);
								clientOutput.writeObject(usernames);
								clientOutput.flush();
								validUsername = false;
							}
							i++;
						}
					}
				} catch (ClassNotFoundException e) {
					// We won't encounter this error unless there's a coding issue
				} catch (IOException e) {
					continueCommunicatingWithClient = false;
				}
			}
			// signal to client that username was accepted
			if (continueCommunicatingWithClient) {
				try {
					clientOutput.writeBoolean(true);
					clientOutput.flush();
					
				} catch (IOException e) {
					continueCommunicatingWithClient = false;
				}
				if (continueCommunicatingWithClient) {
					client.setUsername(username);
					addConnectedClient(client);
					String message = client.getUsername() + " has joined the server on " + (new Date()).toString();
					sendServerMessage(message);
					Connect connectClientToClient = new Connect(client);
					connectClientToClient.run();
				}
			}
		} // close run

	} // close Login
	
	class Connect implements Runnable {
		
		private ConnectedClient client;
		
		public Connect(ConnectedClient c) {
			client = c;
		}
		
		public void run() {
			boolean clientDisconnected = false;
			int assignedNumber;
			ObjectOutputStream clientOutput = client.getOutput();
			synchronized (assignNumberLock) { // synchronized so only one person is incrementing and receiving a number at a time
				takeANumber++;
				assignedNumber = takeANumber;
			}
			try {
				while (assignedNumber != nowServing) { // this loop helps prevent small "traffic jams" while a client is not being given another partner
					Thread.sleep(100);
				}
			} catch (InterruptedException e) {}
			if (openPair == null) { // This means that this client is the first to show up in the pair
				openPair = new ConnectedClient[2];
				openPair[0] = client; // this particular client gets assigned to the first spot in the array
				openPair[1] = null;
				try {
					sendMessage(clientOutput, "Waiting for another client to connect.");
				} catch (IOException e) { // makes sure that the client hasn't disconnected
					removeConnectedClient(client);
					String message = client.getUsername() + " has left the server on " + (new Date()).toString();
					sendServerMessage(message);
					clientDisconnected = true;
					openPair = null;
				}
				nowServing++; // increments this number to give someone else a chance at connecting
				/*
				 * This client is now waiting until a second client connects. When the second client does connect,
				 * things will move fast in the second client's stack, so this boolean, waitingOnFirstClientToEndHeartbeat
				 * will keep the second client waiting until it is time to move on to game mode (GameMode class)
				 */
				waitingOnFirstClientToEndHeartbeat = true;
				try {
					while (!secondClientConnected) { // waits for another client to connect
						clientOutput.writeInt(0);
						clientOutput.flush();
						try {
							Thread.sleep(1000);
						} catch (InterruptedException ee) {}
					}
				} catch (IOException e) { // in case this client disconnects early
					openPair = null;
					String message = client.getUsername() + " has left the server on " + (new Date()).toString();
					sendServerMessage(message);
					removeConnectedClient(client);
				}
				waitingOnFirstClientToEndHeartbeat = false; // signals that the second client may proceed in going to the game
			} else { // indicates that this person is connecting after the first client creates the openPair array
				secondClientConnected = true;
				openPair[1] = client; // assigned to second slot in array
				try {
					sendMessage(clientOutput, "Notifying the waiting client.");
				} catch (IOException e) { // makes sure that this client doesn't
					removeConnectedClient(client);
					String message = client.getUsername() + " has left the server on " + (new Date()).toString();
					sendServerMessage(message);
					clientDisconnected = true;
					openPair[1] = null;
				}
				if (!clientDisconnected) {
					boolean opponentDisconnected = false;
					try {
						sendMessage(clientOutput, "Connected with " + openPair[0].getUsername());
						try {
							Thread.sleep(150);
						} catch (InterruptedException ee) {}
						sendMessage(openPair[0].getOutput(), "Connected with " + client.getUsername());
						beginGame(clientOutput);
						beginGame(openPair[0].getOutput());
					} catch (IOException e) { // some complex disconnect handling
						try {
							sendMessage(clientOutput, "Your opponent has disconnected unexpectedly.");
							// The program will not cross this line if client has disconnected
							removeConnectedClient(openPair[0]);
							String message = openPair[0].getUsername() + " has left an unstarted game with " + client.getUsername() + " on " + (new Date()).toString();
							sendServerMessage(message);
							openPair = null;
							opponentDisconnected = true;
							nowServing++;
							Connect tryAgain = new Connect(client);
							tryAgain.run();
						} catch (IOException ee) {
							try {
								sendMessage(openPair[0].getOutput(), "Your opponent has disconnected unexpectedly.");
								// The program will not cross this line if openPair[0] has disconnected
								removeConnectedClient(client);
								String message = client.getUsername() + " has left an unstarted game with " + openPair[0].getUsername() + " on " + (new Date()).toString();
								sendServerMessage(message);
								clientDisconnected = true;
								nowServing++;
								Connect tryAgain = new Connect(openPair[0]);
								openPair = null;
								tryAgain.run();
							} catch (IOException eee) {
								nowServing++;
								opponentDisconnected = clientDisconnected = true;
								removeConnectedClient(openPair[0]);
								removeConnectedClient(client);
								String message = client.getUsername() + " and " + openPair[0].getUsername() + " have left their unstarted game on " + (new Date()).toString();
								sendServerMessage(message);
								openPair = null;
							}
						}
					}
					while (waitingOnFirstClientToEndHeartbeat) { // proceed once the first client has gotten out of the heartbeat
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {}
					}
					secondClientConnected = false;
					if (!(opponentDisconnected || clientDisconnected)) { // Fancy boolean logic for "both clients are still connected"
						addInGamePair(openPair);
						ConnectedClient[] tempArray = openPair;
						openPair = null;
						nowServing++;
						Thread gameThread = new Thread(new StartGame(tempArray));
						gameThread.setName("GameMode (" + tempArray[0].getUsername() + " and " + tempArray[1].getUsername() + ")");
						gameThread.start();
						/*
						 * The second client's thread dies and a new thread is created to run the game, thus preventing any old
						 * data from accidentally crossing into a new game and keeping one thread's job as one thread's job.
						 */
					}
				}
			}
		} // close run
		
	} // close Connect
	
	// handles a disconnect while a game is being played.
	public void inGamePairDisconnected(ConnectedClient disconnectedClient, boolean bothDisconnected) {
		ConnectedClient[] memberArray = null; 
		int i = 0;
		boolean clientFound = false;
		// searches and removes the pair array
		while (i < inGameClients.size() && !clientFound) {
			ConnectedClient[] tmpInGameClientArray = inGameClients.get(i);
			int j = 0;
			while (j < 2 && !clientFound) {
				if (disconnectedClient.equals(tmpInGameClientArray[j])) {
					clientFound = true;
					memberArray = tmpInGameClientArray;
				}
				j++;
			}
			i++;
		}
		removeConnectedClient(disconnectedClient);
		removeInGamePair(memberArray);
		int otherMemberPosition = 0;
		if (memberArray != null) {
			if (memberArray[0].equals(disconnectedClient)) {
				otherMemberPosition = 1;
			}
			if (bothDisconnected) {
				removeConnectedClient(memberArray[otherMemberPosition]);
				String message = disconnectedClient.getUsername() + " and " + memberArray[otherMemberPosition].getUsername() + " have both left their game on " + (new Date()).toString();
				sendServerMessage(message);
			} else { // if one client is still online, the program moves to reconnect the online client
				ObjectOutputStream otherMemberOutput = memberArray[otherMemberPosition].getOutput();
				String date = (new Date()).toString();
				try {
					sendMessage(otherMemberOutput, disconnectedClient.getUsername() + " has disconnected.");
					String message = disconnectedClient.getUsername() + " has left their game with " + memberArray[otherMemberPosition].getUsername() + " on " + date;
					sendServerMessage(message);
					reconnectClient(memberArray[otherMemberPosition]);
				} catch (IOException e) {
					removeConnectedClient(memberArray[otherMemberPosition]);
					String message = disconnectedClient.getUsername() + " and " + memberArray[otherMemberPosition].getUsername() + " have both left their game on " + date;
					sendServerMessage(message);
				}
			}
		}
	} // close inGamePairDisconnected
	
	// Used for when an error has occured and no other time
	private void reconnectClient(ConnectedClient client) {
		try {
			ObjectOutputStream clientOutput = client.getOutput();
			clientOutput.writeInt(5);
			clientOutput.flush();
			Thread connectClientToClient = new Thread(new Connect(client));
			connectClientToClient.setName("Connect Thread (reconnectClient)");
			connectClientToClient.start();
		} catch (IOException e) {
			removeConnectedClient(client);
		}
	}
	
	// send messages whenever
	public static void sendMessage(ObjectOutputStream clientOutput, String message) throws IOException {
		clientOutput.writeInt(2);
		clientOutput.writeObject(message);
		clientOutput.flush();
	} // close sendMessage
		
	public static void sendServerMessage(String message) {
		if (message.length() > 1 && message.substring(0, 1).equals("/")) {
			if (message.length() > 4 && message.substring(1,5).equalsIgnoreCase("ban ")) {
				String username = message.substring(5);
				if (username.contains(" ")) {
					String[] chopped = username.split(" ", 2);
					username = chopped[0];
				}
				int i = 0;
				int length = connectedClients.size();
				ConnectedClient clientToBan = null;
				while (i < length && clientToBan == null) {
					if (connectedClients.get(i).getUsername().equalsIgnoreCase(username)) {
						clientToBan = connectedClients.get(i);
					}
					else {
						++i;
					}
				}
				if (clientToBan != null) {
					bannedClients.add(new BannedClient(clientToBan));
					try {
						clientToBan.getOutput().writeInt(12);
						clientToBan.getOutput().flush();
					} catch (IOException e) {}
				} else {
					gui.updateChatBox("[**]: The client-to-ban's username was not found.");
				}
				// close /ban
			} else if (message.length() > 6 && message.substring(1, 7).equalsIgnoreCase("unban ")) {
				String username = message.substring(7);
				if (username.contains(" ")) {
					String[] chopped = username.split(" ", 2);
					username = chopped[0];
				}
				int i = 0;
				int length = bannedClients.size();
				boolean clientFound = false;
				while (i < length && !clientFound) {
					if (bannedClients.get(i).getUsername().equalsIgnoreCase(username)) {
						username = bannedClients.get(i).getUsername();
						bannedClients.remove(i);
						gui.updateChatBox("[**]: " + username + " has been unbanned and may now join the server.");
						clientFound = true;
					} else {
						++i;
					}
				}
				if (!clientFound) 
					gui.updateChatBox("[**]: The client-to-unban's username was not found.");
				// close /unban
			} else if (message.length() > 9 && message.substring(1, 10).equalsIgnoreCase("checkbans")) {
				if (bannedClients.size() == 0) {
					message = "[**]: There are no banned users.";
					gui.updateChatBox(message);
				} else {
					message = "[**]: BANNED USERS:\n";
					for (int i = 0; i < bannedClients.size(); ++i) {
						message += ("\n-- " + bannedClients.get(i).getUsername());
					}
					message += "\n";
					gui.updateChatBox(message);
				}
				// close /checkbans
			} else if (message.length() > 4 && message.substring(1, 5).equalsIgnoreCase("time")) {
				server.printUpTime(false);
				// close /time
			} else if (message.length() > 4 && message.substring(1, 5).equalsIgnoreCase("help")) {
				for (serverCommand c : serverCommand.values()) {
					message = "*\t" + c.toString() + "\n* DESCRIPTION: " + c.description() + "\n* USAGE: " + c.usage() + "\n";
					gui.updateChatBox(message);
				}
				// close /help
			} else if (message.length() > 4 && message.substring(1, 5).equalsIgnoreCase("msg ")) {
				String username = message.substring(5);
				if (username.contains(" ")) {
					String[] chopped = username.split(" ", 2);
					username = chopped[0];
					message = chopped[1];
				} else {
					gui.updateChatBox("[**]: Invalid username or message. Your message was not sent.");
					return;
				}
				int i = 0;
				int length = connectedClients.size();
				ConnectedClient clientToMessage = null;
				while (i < length && clientToMessage == null) {
					if (connectedClients.get(i).getUsername().equalsIgnoreCase(username))
						clientToMessage = connectedClients.get(i);
					else
						++i;
				}
				if (clientToMessage != null) {
					message = "[SERVER -> " + clientToMessage.getUsername() + "]: " + message;
					gui.updateChatBox(message);
					try {
						sendMessage(clientToMessage.getOutput(), message);
					} catch (IOException e) {}
				} else {
					gui.updateChatBox("[**]: The intended recipient's username was not found.");
				}
				// close /msg
			} else {
				gui.updateChatBox("[**]: COMMAND NOT FOUND.");
			} // close command not found
		} else {
			message = "[SERVER]: " + message;
			gui.updateChatBox(message);
			for (ConnectedClient client : connectedClients) {
				try {
					sendMessage(client.getOutput(), message);
				} catch (IOException e) {}
			}
		}
	} // close sendServerMessage
	
	public static void sendClientMessage(ConnectedClient sending, ConnectedClient receiving, String message) {
		if (message.length() > 1 && message.substring(0, 1).equals("/")) {
			if (message.length() > 4 && message.substring(1, 5).equalsIgnoreCase("help")) {
				message = "\n";
				for (clientCommand c : clientCommand.values()) {
					message += ("*\t" + c.toString() + "\n* DESCRIPTION: " + c.description() + "\n* USAGE: " + c.usage() + "\n");
				}
				try {
					sendMessage(sending.getOutput(), message);
				} catch (IOException e) {}
				// close /help
			} else if (message.length() > 4 && message.substring(1, 5).equalsIgnoreCase("msg ")) {
				String username = message.substring(5);
				if (username.contains(" ")) {
					String[] chopped = username.split(" ", 2);
					username = chopped[0];
					message = chopped[1];
				} else {
					try {
						sendMessage(sending.getOutput(), "[SERVER]: Invalid username or message. Your message was not sent.");
					} catch (IOException e) {}
					return;
				}
				if (username.equalsIgnoreCase("server")) {
					message = "[" + sending.getUsername() + " -> SERVER]: " + message;
					gui.updateChatBox(message);
					return;
				}
				int i = 0;
				int length = connectedClients.size();
				ConnectedClient clientToMessage = null;
				while (i < length && clientToMessage == null) {
					if (connectedClients.get(i).getUsername().equalsIgnoreCase(username))
						clientToMessage = connectedClients.get(i);
					else
						++i;
				}
				if (clientToMessage != null) {
					message = "[" + sending.getUsername() + " -> " + clientToMessage.getUsername() + "]: " + message;
					gui.updateChatBox(message);
					try {
						sendMessage(sending.getOutput(), message);
						sendMessage(clientToMessage.getOutput(), message);
					} catch (IOException e) {}
				} else {
					try {
						sendMessage(sending.getOutput(), "[SERVER]: The intended recipient's username was not found.");
					} catch (IOException e) {}
				}
				// close /msg
			} else if (message.length() > 11 && message.substring(1, 12).equalsIgnoreCase("onlineusers")) {
				message = "\nONLINE CLIENTS: \n\t***\n";
				for (ConnectedClient client : connectedClients) {
					message += ("\t" + client.getUsername() + "\n");
				}
				message += "\t***";
				try {
					sendMessage(sending.getOutput(), message);
				} catch (IOException e) {}
			} else {
				try {
					sendMessage(sending.getOutput(), "[SERVER]: COMMAND NOT FOUND.");
				} catch (IOException e) {}
			} // close command not found
		} else {
			message = "[" + sending.getUsername() + "]: " + message;
			try {
				sendMessage(receiving.getOutput(), message);
			} catch (IOException e) {}
			gui.updateChatBox(message);
		}
	} // close sendClientMessage
	
	// code to let a client know a game is beginning
	private void beginGame(ObjectOutputStream clientOutput) throws IOException {
		clientOutput.writeInt(6);
		clientOutput.flush();
	} // close beginGame
	
	// returns the only instance of this class
	public static MainConnect getServer() {
		return server;
	} // close getServer
	
	// used for when a game has successfully completed
	public void restartPair(GameMode gameData) {
		ConnectedClient client1 = gameData.getClient1();
		ConnectedClient client2 = gameData.getClient2();
		boolean pairFound = false;
		int i = 0;
		while (i < inGameClients.size() && !pairFound) {
			int j = 0;
			while (j < 2 && !pairFound) {
				if (inGameClients.get(i)[j].equals(client1)) {
					removeInGamePair(inGameClients.get(i));
					pairFound = true;
				}
				j++;
			}
			i++;
		}
		
		Thread connectC1 = new Thread(new Connect(client1));
		Thread connectC2 = new Thread(new Connect(client2));
		connectC1.setName("Connect Thread (restartPair)");
		connectC2.setName("Connect Thread (restartPair)");
		gameData.checkIfChatIsConnected(client1); // waits to make sure that the old chat thread was killed
		connectC1.start(); // puts the client back to the top of the sorting pile to be reconnected
		gameData.checkIfChatIsConnected(client2);
		connectC2.start();
		/*
		 * Once this method has ended, the thread which originally started the game (the second client's thread)
		 * dies as it is no longer needed to keep the game running.
		 */
	} // close restartPair
	
	private void addConnectedClient(ConnectedClient client) {
		connectedClients.add(client);
		gui.updateConnectedClientsList(connectedClients);
	} // close addConnectedClient
	
	private void removeConnectedClient(ConnectedClient client) {
		connectedClients.remove(client);
		gui.updateConnectedClientsList(connectedClients);
	} // close removeConnectedClient
	
	private void addInGamePair(ConnectedClient[] pair) {
		inGameClients.add(pair);
		gui.updateInGameClientsList(inGameClients);
	} // close addInGamePair
	
	private void removeInGamePair(ConnectedClient[] pair) {
		inGameClients.remove(pair);
		gui.updateInGameClientsList(inGameClients);
	} // close removeInGamePair
	
	public void disconnect() {
		try {
			serverSocket.close();
		} catch (IOException e) {}
		gui.setRunning(false);
		printUpTime(true);
		gui.updateChatBox("[**]: The server is shutting down on " + (new Date()).toString());
		disconnectClients();
		clearOldData();
		gui.updateChatBox("[**]: SERVER HAS GONE OFFLINE AND CLIENTS HAVE BEEN DISCONNECTED.");
	} // close disconnect
	
	private void printUpTime(boolean shuttingDown) {
		String presentOrPast = shuttingDown ? "was" : "has been";
		long endTime = System.currentTimeMillis();
		int timeRunningSeconds = (int) ((endTime - startTime)/1000);
		if (timeRunningSeconds < 60) {
			gui.updateChatBox("[**]: The server " + presentOrPast + " running for " + timeRunningSeconds + " second" + (timeRunningSeconds > 1 ? "s." : "."));
		} else if (timeRunningSeconds < 3600) {
			int minutes = timeRunningSeconds / 60;
			int seconds = timeRunningSeconds % 60;
			gui.updateChatBox("[**]: The server " + presentOrPast + " running for " + minutes + " minute" + (minutes > 1 ? "s" : "") + " and " + seconds + " second" + (seconds > 1 ? "s" : "") + ". (" + minutes + ":" + (seconds < 10 ? "0" : "") + seconds + ")");
		} else if (timeRunningSeconds < 86400) {
			int hours = timeRunningSeconds / 3600;
			int minutes = (timeRunningSeconds % 3600) / 60;
			int seconds = timeRunningSeconds % 60;
			gui.updateChatBox("[**]: The server " + presentOrPast + " running for " + hours + "hour" + (hours > 1 ?  "s" : "") + minutes + ", minute" + (minutes > 1 ? "s" : "") + ", and " + seconds + " second" + (seconds > 1 ? "s" : "") + ". (" + hours + ":" + (minutes < 10 ? "0" : "") + minutes + ":" + (seconds < 10 ? "0" : "") + seconds + ")");
		} else {
			int days = timeRunningSeconds / 86400;
			int hours = (timeRunningSeconds % 86400) / 3600;
			int minutes = (timeRunningSeconds % 3600) / 60;
			int seconds = timeRunningSeconds % 60;
			gui.updateChatBox("[**]: The server " + presentOrPast + " running for " + days + " day" + (days > 1 ? "s" : "") + ", " + hours + "hour" + (hours > 1 ? "s" : "") + ", " + minutes + " minute" + (minutes > 1 ? "s" : "") + ", and " + seconds + " second" + (seconds > 1 ? "s" : "") + ". (" + days + ":" + (hours < 10 ? "0" : "") + hours + ":" + (minutes < 10 ? "0" : "") + minutes + ":" + (seconds < 10 ? "0" : "") + seconds + ")");
		}
	} // close calculateUpTime
	
	private void clearOldData() {
		connectedClients = new ArrayList<ConnectedClient>(); 
		inGameClients = new ArrayList<ConnectedClient[]>();
		takeANumber = 0;
		nowServing = 1;
		openPair = null;
		secondClientConnected = false;
		waitingOnFirstClientToEndHeartbeat = false;
		serverSocket = null;
	} // close clearOldData
	
	private void disconnectClients() {
		int length = connectedClients.size();
		for (int i = 0; i < length; i++) {
			ObjectOutputStream clientOutput = connectedClients.get(i).getOutput();
			try {
				clientOutput.writeInt(11);
				clientOutput.flush();
			} catch (IOException e) {}
		}
	} // close disconnectClients
	
	class StartGame implements Runnable {
		
		private ConnectedClient[] gamers;
		
		StartGame(ConnectedClient[] pair) {
			gamers = pair;
		}

		public void run() {
			GameMode newGame = new GameMode(gamers);
			newGame.startGame(gamers);
		} // close run
		
	} // close StartGame
	
} // close MainConnect