package rbattleshipServer;

import java.net.InetAddress;

public class BannedClient {
	
	private String username;
	private InetAddress ip;
	
	public BannedClient(ConnectedClient client) {
		username = client.getUsername();
		ip = client.getIP();
	} // close constructor();
	
	public String getUsername() {
		return username;
	} // close getUsername
	
	public InetAddress getIP() {
		return ip;
	} // close getIP
	
} // close BannedClient
