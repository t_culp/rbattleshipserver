package rbattleshipServer;

import java.io.*;
import java.util.Date;

public class GameMode {
	
	private ConnectedClient client1;
	private ConnectedClient client2;
	private boolean disconnected = false;
	private String guess = null;
	private boolean client1Disconnected = false;
	private boolean client2Disconnected = false;
	private MainConnect server = MainConnect.getServer();
	private Thread client1Chat;
	private Thread client2Chat;
	
	public GameMode(ConnectedClient[] pair) {
		client1 = pair[0];
		client2 = pair[1];
	} // close constructor
	
	public void startGame(ConnectedClient[] pair) {
		String message = client1.getUsername() + " and " + client2.getUsername() + " have started their game on " + (new Date()).toString();
		MainConnect.sendServerMessage(message);
		client1.setBattleships(createBattleships());
		client2.setBattleships(createBattleships());
		/*
		 * Chat listeners are created to keep an eye on incoming messages as well as managing all other 
		 * communications, such as selected positions and hits.
		 */
		client1Chat = new Thread(new ChatListener(client1.getInput()));
		client2Chat = new Thread(new ChatListener(client2.getInput()));
		client1Chat.setName("Chat Listener (" + client1.getUsername() + ")");
		client1Chat.start();
		client2Chat.setName("Chat Listener (" + client2.getUsername() + ")");
		client2Chat.start();
		try {
			MainConnect.sendMessage(client1.getOutput(), "You go first.");
		} catch (IOException e) {
			disconnected = true;
		}
		try {
			MainConnect.sendMessage(client2.getOutput(), "It's your turn when your opponent has made a selection");
		} catch (IOException e) {
			disconnected = true;
		}
		try {
			while (!disconnected) { // a loop of turn after turn until the clients become disconnected
				clientTurn(client1);
				Thread.sleep(100);
				if (!disconnected) {
					clientTurn(client2);
					Thread.sleep(100);
				}
			}
		} catch (InterruptedException ex) {}
	} // close startGame
	
	/*
	 * DATA ENCODING: 
	 * int x = 0 represents end of game, details following
	 * int x = 1 battleship string following 
	 * int x = 2 send your battleship choice
	 * int x = 3 chat string following
	 */
	
	private void clientTurn(ConnectedClient client) {
		ObjectOutputStream clientOutput = client.getOutput();
		// This is to determine which client is the client not being currently communicated with
		ConnectedClient opposingClient = null;
		if (client.equals(client1))
			opposingClient = client2;
		else
			opposingClient = client1;
		ObjectOutputStream opposingClientOutput = opposingClient.getOutput();
		String selectedPosition = null;
		boolean hit = false;
		try {
			// prompt user for battleship choice
			sendBattleshipPosition(clientOutput);
			while (guess == null) { // waits for choice
				Thread.sleep(100);
				if (client1Disconnected || client2Disconnected)
					break;
			}
			if (client1Disconnected && client2Disconnected) { // both clients have disconnected
				disconnected = true;
				server.inGamePairDisconnected(client, true);
			} else if (client1Disconnected || client2Disconnected) { // one client has disconnected
				disconnected = true;
				MainConnect informDisconnection = new MainConnect();
				ConnectedClient disconnectedClient = client1Disconnected ? client1 : client2;
				ConnectedClient remainingClient = client1Disconnected ? client2 : client1;
				try {
					MainConnect.sendMessage(remainingClient.getOutput(), "Connection with your opponent has been lost. You will now go back to the lobby.");
					informDisconnection.inGamePairDisconnected(disconnectedClient, false); 
				} catch (IOException e) { // if even more goes wrong
					informDisconnection.inGamePairDisconnected(disconnectedClient, true);
				}
			} else { // both clients are still connected
				selectedPosition = guess;
				guess = null;
				hit = checkGuess(selectedPosition, client);
				// sending hit
				sendHit(clientOutput, hit);
			}
		} catch (IOException e) { // disconnected client
			disconnected = true;
			try {
				MainConnect.sendMessage(opposingClientOutput, "Connection with your opponent has been lost. You will now go back to the lobby.");
				server.inGamePairDisconnected(client, false);
				// the false argument means that the 2nd client remains connected.
			} catch (IOException ee) {
				server.inGamePairDisconnected(client, true);
				//the true argument means that the 2nd client has disconnected.
			}
		} catch (InterruptedException e) {}
		if (!disconnected) {
			try {
				// send out data from this client to the opposing client
				readPositionAndHit(opposingClientOutput, hit, selectedPosition);
				if (hit) {
					boolean singleShipDestroyed = checkSingleShipDestroyed(opposingClient, selectedPosition);
					signalBattleshipDestroyed(opposingClientOutput, singleShipDestroyed);
					signalBattleshipDestroyed(clientOutput, singleShipDestroyed);
					if (singleShipDestroyed) {
						boolean allShipsDestroyed = checkAllShipsDestroyed(opposingClient);
						if (allShipsDestroyed)
							finishGame(opposingClient);
						 // close allShipsDestroyed
					} // close close singleShipDestroyed
				} // close hit
			} catch (IOException ex) {
				disconnected = true;
				try {
					MainConnect.sendMessage(clientOutput, "Connection with your opponent has been lost. You will now go back to the lobby.");
					server.inGamePairDisconnected(opposingClient, false);
					// the false argument means that the 2nd client remains connected.
				} catch (IOException ee) {
					server.inGamePairDisconnected(opposingClient, true);
					//the true argument means that the 2nd client has disconnected.
				}
			} 
		} // close if (!disconnected)
	} // close clientTurn
	
	private void sendBattleshipPosition(ObjectOutputStream clientOutput) throws IOException {
		clientOutput.writeInt(7); // signals to send the battleship position selected
		clientOutput.flush();
	} // close sendBattleshipPosition
	
	private void sendHit(ObjectOutputStream clientOutput, boolean hit) throws IOException {
		clientOutput.writeInt(8); // signals hit data is on the way
		clientOutput.writeInt(hit ? 1 : 2); // 1 if true, 2 if false
		clientOutput.flush();
	} // close sendHit
	
	private void readPositionAndHit(ObjectOutputStream clientOutput, boolean hit, String selectedPosition) throws IOException {
		clientOutput.writeInt(3); // start readGuess Listener
		clientOutput.writeInt(8); // prepare to read hit
		clientOutput.writeInt(hit ? 1 : 2); // sends hit
		clientOutput.writeInt(10); // prepare to read guess
		clientOutput.writeObject(selectedPosition); // sends guess
		clientOutput.flush();
	} // close readPositionAndHit
	
	private void signalBattleshipDestroyed(ObjectOutputStream clientOutput, boolean destroyed) throws IOException {
		clientOutput.writeInt(9);
		clientOutput.writeInt(destroyed ? 1 : 2);
		clientOutput.flush();
	} // close signalBattleshipDestroyed
	
	// gives random battleships to each player
	private static BattleshipObjects[] createBattleships() {
		BattleshipObjects b1 = new BattleshipObjects();
		BattleshipObjects b2 = new BattleshipObjects();
		BattleshipObjects b3 = new BattleshipObjects();
		BattleshipObjects[] battleships = {b1, b2, b3};
		battleships = BattleshipObjects.checkPositions(battleships[0], battleships[1], battleships[2]);
		return battleships;
	} // close createBattleships
	
	// checks if a battleship has been hit
	private boolean checkGuess(String guess, ConnectedClient client){
		boolean hit = false;
		BattleshipObjects hitBattleship = null;
		ConnectedClient opposingClient = null;
		if (client.equals(client1))
			opposingClient = client2;
		else
			opposingClient = client1;
		int battleshipIndex = 0;
		int positionIndex = 0;
		while (battleshipIndex < 3 && !hit) {
			positionIndex = 0;
			hitBattleship = opposingClient.getBattleships()[battleshipIndex];
			while (positionIndex < 3 && !hit) {
				if (hitBattleship.getPosition(positionIndex).equals(guess)) {
					hitBattleship.setDestroyed(positionIndex);
					hit = true;
				}
				positionIndex++;
			}
			battleshipIndex++;
		}
		return hit;
	} // close checkGuess
	
	// checks if just one ship has been destroyed
	private boolean checkSingleShipDestroyed (ConnectedClient client, String position) {
		int i = 0;
		BattleshipObjects destroyedShip = null;
		while (i < 3 && destroyedShip == null) {
			BattleshipObjects tmpShip = client.getBattleships()[i];
			int j = 0;
			while (j < 3 && destroyedShip == null) {
				if (tmpShip.getPosition(j).equals(position))
					destroyedShip = tmpShip;
				j++;
			}
			i++;
		}
		i = 0;
		boolean destroyed = true;
		while (i < 3 && destroyed) {
			if (!destroyedShip.getDestroyed(i))
				destroyed = false;
			i++;
		}
		return destroyed;
	} // close checkSingleShipSDestroyed
	
	// checks if all ships have been destroyed (which proceeds to end the game in the clientTurn method)
	private boolean checkAllShipsDestroyed(ConnectedClient client) {
		int i = 0;
		boolean destroyed = true;
		while (i < 3 && destroyed) {
			BattleshipObjects tmpShip = client.getBattleships()[i];
			int j = 0;
			while (j < 3 && destroyed) {
				if (!tmpShip.getDestroyed(j))
					destroyed = false;
				j++;
			}
			i++;
		}
		return destroyed;
	} // close checkAllShipsDestroyed
	
	// ends the game if it is completed
	private void finishGame(ConnectedClient loser) throws IOException {
		disconnected = true;
		ObjectOutputStream loserOutput = loser.getOutput();
		loserOutput.writeInt(4); // tells client to kill off chat listener and listen for whether they won or not
		loserOutput.writeBoolean(false);
		loserOutput.flush();
		ConnectedClient winner = null;
		if (loser.equals(client1))
			winner = client2;
		else
			winner = client1;
		String message = winner.getUsername() + " has won their game with " + loser.getUsername() + " on " + (new Date()).toString();
		MainConnect.sendServerMessage(message);
		ObjectOutputStream winnerOutput = winner.getOutput();
		winnerOutput.writeInt(4); // same down here as for the other write 
		winnerOutput.writeBoolean(true);
		winnerOutput.flush();
		server.restartPair(this);
	} // close finishGame
	
	// checks if a chat thread is still running. This function will wait until it has been terminated
	public void checkIfChatIsConnected(ConnectedClient client) {
		if (client.equals(client1)) {
			while (client1Chat.isAlive()){
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {}
			}
		}
		else {
			while (client2Chat.isAlive()) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {}
			}
		}
	} // close checkIfChatIsConnected
	
	// returns client1's instance
	public ConnectedClient getClient1() {
		return client1;
	} // close getClient1
	
	// returns client2's instance
	public ConnectedClient getClient2() {
		return client2;
	} // close getClient2
	
	class ChatListener implements Runnable {
		
		private ObjectInputStream clientInput;
		
		public ChatListener(ObjectInputStream ci) {
			clientInput = ci;
		} // close constructor
		
		/*
		 * DATA ENCODING:
		 * true: Regular data follows (a guess)
		 * false: message follows
		 */
		
		public void run() {
			boolean continueLoop = true;
			int code = 0;
			while (code != 2 && continueLoop) { // if the code equals 2, this thread will die.
				try {
					code = clientInput.readInt();
					if (code == 0) {
						guess = (String) clientInput.readObject();
					} else if (code == 1) {
						ConnectedClient sending = null;
						ConnectedClient receiving = null;
						if (clientInput.equals(client1.getInput())) {
							sending = client1;
							receiving = client2;
						} else {
							sending = client2;
							receiving = client1;
						}
						String message = (String) clientInput.readObject();
						MainConnect.sendClientMessage(sending, receiving, message);
					}
				} catch (IOException e) {
					if (clientInput.equals(client1.getInput()))
						client1Disconnected = true;
					else
						client2Disconnected = true;
					continueLoop = false;
				} catch (ClassNotFoundException e) {}
			} // close while code != 2
		} // close run
		
	} // close ChatListener
	
} // close GameMode