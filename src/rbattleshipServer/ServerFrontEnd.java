package rbattleshipServer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.DefaultCaret;

public class ServerFrontEnd {
	
	private JFrame frame;
	private MainConnect server;
	private JTextArea chatBox;
	private JTextField chatEntry;
	private JButton send;
	private JButton start;
	DefaultListModel<String> connectedClientsListModel = new DefaultListModel<String>();
	DefaultListModel<String> inGameClientsListModel = new DefaultListModel<String>();

	public static void main(String[] args) {
		// main used for just looking at the GUI, uses the constructor directly below
		(new ServerFrontEnd()).createGUI();
	} // close main
	
	public ServerFrontEnd() {
		/*
		 * Constructor used for just GUI testing, the constructor below must be used for starting the
		 * server with this front end
		 */
	}
	
	public ServerFrontEnd(MainConnect server) {
		this.server = server;
		createGUI();
	} // close constructor
	
	private void createGUI() {
		// frame creation
		frame = new JFrame("Battleship Server");
		frame.setLayout(new GridBagLayout());
		frame.setSize(1000, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagConstraints gc = new GridBagConstraints();
		
		// panels: chat, connected clients, in-game pairs
		JPanel chatPanel = new JPanel(new GridBagLayout());
		chatPanel.setPreferredSize(new Dimension(575, 560));
		//chatPanel.setBackground(Color.RED);
		JPanel clientPanel = new JPanel(new GridBagLayout());
		clientPanel.setPreferredSize(new Dimension(200, 560));
		//clientPanel.setBackground(Color.GREEN);
		JPanel pairsPanel = new JPanel(new GridBagLayout());
		pairsPanel.setPreferredSize(new Dimension(200, 560));
		//pairsPanel.setBackground(Color.BLUE);
		gc.gridx = 0;
		gc.gridy = 0;
		gc.weightx = 0.26;
		frame.getContentPane().add(chatPanel, gc);
		gc.gridx = 1;
		gc.weightx = 0.1;
		frame.getContentPane().add(clientPanel, gc);
		gc.gridx = 2;
		frame.getContentPane().add(pairsPanel, gc);
		
		// chat panel
		chatBox = new JTextArea(35, 45);
		gc.gridx = 0;
		gc.gridwidth = 3;
		gc.weighty = 0.99;
		gc.fill = GridBagConstraints.BOTH;
		chatBox.setEditable(false);
		chatBox.setLineWrap(true);
		chatBox.setWrapStyleWord(true);
		DefaultCaret caret = (DefaultCaret) chatBox.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		JScrollPane scroller = new JScrollPane(chatBox);
		scroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		//scroller.getViewport().setScrollMode(JViewport.SIMPLE_SCROLL_MODE);
		chatPanel.add(scroller, gc);
		chatBox.setMaximumSize(chatBox.getPreferredSize());
		chatPanel.setMaximumSize(chatPanel.getPreferredSize());
		
		// adding chatEntry
		chatEntry = new JTextField();
		chatEntry.setEnabled(false);
		chatEntry.setFocusable(true);
		chatEntry.addKeyListener(new EnterKeyListener());
		gc.gridx = 0;
		gc.gridy = 1;
		gc.weighty = 0.01;
		gc.gridheight = 1;
		gc.gridwidth = 1;
		gc.weightx = 0.95;
		chatPanel.add(chatEntry, gc);
		
		// adding send button (reuses some old gc)
		send = new JButton("Send");
		send.addActionListener(new SendListener());
		send.setEnabled(false);
		gc.gridx = 1;
		gc.weightx = 0.01;
		chatPanel.add(send, gc);
		
		// adding start button
		start = new JButton("Start");
		start.addActionListener(new StartListener());
		gc.gridx = 2;
		chatPanel.add(start, gc);
		
		// clients panel
		gc.gridx = 0;
		gc.gridy = 0;
		gc.weighty = 0.05;
		JLabel connectedClientsListLabel = new JLabel("Connected Clients");
		connectedClientsListLabel.setHorizontalTextPosition(JLabel.CENTER);
		connectedClientsListLabel.setHorizontalAlignment(JLabel.CENTER);
		connectedClientsListLabel.setForeground(Color.BLUE);
		clientPanel.add(connectedClientsListLabel, gc);
		JList<String> connectedClientsList = new JList<String>(connectedClientsListModel);
		JScrollPane connectedClientsScroller = new JScrollPane(connectedClientsList);
		gc.gridy = 1;
		gc.weighty = 0.9;
		connectedClientsScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		connectedClientsScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		connectedClientsScroller.getViewport().setScrollMode(JViewport.SIMPLE_SCROLL_MODE);
		clientPanel.add(connectedClientsScroller, gc);
		
		// in game clients panel
		gc.gridy = 0;
		gc.weighty = 0.05;
		JLabel inGameClientsListLabel = new JLabel("In-Game Clients");
		inGameClientsListLabel.setHorizontalTextPosition(JLabel.CENTER);
		inGameClientsListLabel.setHorizontalAlignment(JLabel.CENTER);
		inGameClientsListLabel.setForeground(Color.GREEN);
		pairsPanel.add(inGameClientsListLabel, gc);
		JList<String> inGameClientsList = new JList<String>(inGameClientsListModel);
		JScrollPane inGameClientsScroller = new JScrollPane(inGameClientsList);
		gc.gridy = 1;
		gc.weighty = 0.9;
		inGameClientsScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		inGameClientsScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		inGameClientsScroller.getViewport().setScrollMode(JViewport.SIMPLE_SCROLL_MODE);
		pairsPanel.add(inGameClientsScroller, gc);
		
		frame.pack();
		frame.setVisible(true);
	} // close createGUI
	
	public void updateInGameClientsList(ArrayList<ConnectedClient[]> inGameClients) {
		inGameClientsListModel.clear();
		int length = inGameClients.size();
		for (int i = 0; i < length; i++) {
			ConnectedClient[] pair = inGameClients.get(i);
			inGameClientsListModel.addElement(pair[0].getUsername() + " & " + pair[1].getUsername());
		}
	} // updateInGameClientsList
	
	public void updateConnectedClientsList(ArrayList<ConnectedClient> connectedClients) {
		connectedClientsListModel.clear();
		int length = connectedClients.size();
		for (int i = 0; i < length; i++) {
			connectedClientsListModel.addElement(connectedClients.get(i).getUsername());
		}
	} // close updateConnectedClientsList
	
	public void updateChatBox(String message) {
		chatBox.append(message + "\n");
	} // updateChatBox
	
	public String getPort() {
		String port = JOptionPane.showInputDialog("Enter port number (1024 - 65535): \n<enter> = 2014");
		return port;
	} // close getPort
	
	public void showErrorDialog(String message) {
		JOptionPane.showMessageDialog(frame, message, "Error", JOptionPane.ERROR_MESSAGE);
	} // close showErrorDialog
	
	public void showBadPortNumber() {
		JOptionPane.showMessageDialog(frame, "Please input a valid port number", "Error", JOptionPane.ERROR_MESSAGE);
	} // close showBadPortNumber
	
	public void setRunning(boolean running) {
		if (running) {
			start.setText("Stop");
			send.setEnabled(false);
			chatEntry.setEnabled(true);
		} else {
			start.setText("Start");
			send.setEnabled(false);
			chatEntry.setText("");
			chatEntry.setEnabled(false);
		}
	} // close setRunning
	
	private void clearOldData() {
		connectedClientsListModel.clear();
		inGameClientsListModel.clear();
	} // close clearOldData
	
	class SendListener implements ActionListener {

		public void actionPerformed(ActionEvent arg0) {
			String message = null;
			if (!(message = chatEntry.getText()).equals("")) {
				MainConnect.sendServerMessage(message);
				chatEntry.setText("");
				send.setEnabled(false);
			} else {
				send.setEnabled(false);
			}
		} // close actionPerformed
				
	} // close SendListener

	class EnterKeyListener implements KeyListener {
		/*
		 * (non-Javadoc)
		 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
		 * 
		 * This listens for any key while in messageField. It will disable
		 * the send button when there is no text in the box and enable it when
		 * there is. 
		 * It will send the message in messageField when the enter key has been 
		 * released
		 */

		public void keyPressed(KeyEvent arg0) {
			if (chatEntry.getText().equals("")) {
				send.setEnabled(false);
			} else {
				send.setEnabled(true);
			}
		} // close keyPressed

		public void keyReleased(KeyEvent arg0) {
			if (arg0.getKeyCode() == 10 && !chatEntry.getText().equals("")) {
				String message = chatEntry.getText();
				MainConnect.sendServerMessage(message);
				chatEntry.setText("");
				send.setEnabled(false);
			} else {
				if (chatEntry.getText().equals(""))
					send.setEnabled(false);
				else
					send.setEnabled(true);
			}
		} // close keyReleased

		public void keyTyped(KeyEvent arg0) {
			if (chatEntry.getText().equals(""))
				send.setEnabled(false);
			else
				send.setEnabled(true);
		} // close keyTyped
		
	} // close EnterKeyListener
	
	class StartListener implements ActionListener {

		public void actionPerformed(ActionEvent arg0) {
			if (start.getText().equals("Start")) {
				Thread restartThread = new Thread(server.new Restart());
				restartThread.setName("SERVER LOGIN (RESTART)");
				restartThread.start();
			} else {
				server.disconnect();
				clearOldData();
			}
		} // close actionPerformed
		
	} // close StartListener

} // close ServerFrontEnd