package rbattleshipServer;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class ConnectedClient {
	
	private String username;
	private BattleshipObjects[] battleships;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private InetAddress ip;
	
	public ConnectedClient(Socket socket) throws IOException {
		output = new ObjectOutputStream(socket.getOutputStream());
		input = new ObjectInputStream(socket.getInputStream());
		ip = socket.getInetAddress();
	} // close constructor

	public ObjectOutputStream getOutput() {
		return output;
	} // close getOutput
	
	public ObjectInputStream getInput() {
		return input;
	} // close getInput
	
	public void setUsername(String un) {
		username = un;
	} // close setUsername
	
	public String getUsername() {
		return username;
	} // close getUseranem
	
	public void setBattleships(BattleshipObjects[] ships) {
		battleships = ships;
	} // close setBattleships
	
	public BattleshipObjects[] getBattleships() {
		return battleships;
	} // close getBattleships
	
	public InetAddress getIP() {
		return ip;
	} // close getIP
	
} // close ConnectedClient