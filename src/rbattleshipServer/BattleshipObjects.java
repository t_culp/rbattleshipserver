package rbattleshipServer;

public class BattleshipObjects {
	
	private String[] position = new String[3];
	private boolean[] destroyed = {false, false, false}; // Used for understanding which ships have been destroyed
	
	public BattleshipObjects() {
		// creates a battleship parallel to the y-axis
		if (Math.random() < 0.5){
			createVertical();
		}
		// creates a battleship parallel to the x-axis
		else{
			createHorizontal();
		}
	} // close constructor
	
	private void createVertical() {
		char numX = (char) (65 + (int) (Math.random() * 7));
		int numY = (int) (Math.random() * 5) + 1;
		for (int i = 0; i < 3; i++){
			position[i] = "" + numX + (numY + i);
		}
	} // close createVertical
	
	private void createHorizontal() {
		char numX = (char) (65 + (int) (Math.random() * 5));
		int numY = (int) (Math.random() * 7) + 1;
		for (int i = 0; i < 3; i++){
			position[i] = "" + ((char) (numX + i)) + numY;
		}
	} // close createHorizontal
	
	public String getPosition(int index) {
		return position[index];
	} // close getPosition
	
	public boolean getDestroyed(int index) {
		return destroyed[index];
	} // close getDestroyed
	
	public void setDestroyed(int index) {
		destroyed[index] = true;
	} // close setDestroyed
	
	public static BattleshipObjects[] checkPositions(BattleshipObjects b1, BattleshipObjects b2, BattleshipObjects b3){
		/*
		 * Returns an array of 3 battleships which do not intersect each other
		 * given 3 battleships as arguments which may or may not intersect
		 * each other.
		 */
		boolean intersection = true;
		while (intersection){
			intersection = false;
			int i = 0;
			while(!intersection && i < 3){
				int j = 0;
				while(!intersection && j < 3){
					if (b1.getPosition(i).equals(b2.getPosition(j))){
						intersection = true;
					}
					if (b1.getPosition(i).equals(b3.getPosition(j))){
						intersection = true;
					}
					if (b2.getPosition(i).equals(b3.getPosition(j))){
						intersection = true;
					}
					j++;
				}
				i++;
			}
			if (intersection) {
				b1 = new BattleshipObjects();
				b2 = new BattleshipObjects();
				b3 = new BattleshipObjects();
				intersection = true;
			}
		} // close while (intersection)
		BattleshipObjects[] battleships = {b1, b2, b3};
		return battleships;
	} // close checkPositions

} // close BattleshipObjects